# Carbon Credits with Verifiable claims

### This app is a DEMO and is not suited for use in production!!!

Run with docker:
```sh
docker run -p 8080:8080 -v ./data:/data -it kazimuth/carbon-credits-demo
```

Then open [localhost:8080](http://localhost:8080/) in your browser.

Run with npm:
```sh
git clone https://gitlab.com/kazimuth/carbon-credits-demo
cd carbon-credits-demo
npm install -g parcel-bundler
npm install
npm start
```

Then open [localhost:8080](http://localhost:8080/) in your browser.