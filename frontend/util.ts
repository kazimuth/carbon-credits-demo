import m from 'mithril'

export const functionComponent = (view: () => m.Vnode<any, any>) => ({ view })
export const hero = (title: string, subtitle: string, page: string) => 
    m('div', [
        m('section', {class: 'hero'}, m('div', {class: 'hero-body'}, m('div', {class: 'container'}, [
            m('h1', {class: 'title'}, title),
            m('h2', {class: 'subtitle'}, subtitle),
        ]))),
        m('div', {class: `is-${page}-hl thicc-rule`}),
    ])

export type Page = 'acr' | 'project-owner' | 'verifier' | 'public' | 'settings'
export const tabs = ['acr', 'project-owner', 'verifier', 'public', 'spacer', 'settings']
export const icons: {[p: string]: string} = {
    'acr': 'fa-leaf',
    'project-owner': 'fa-tree',
    'verifier': 'fa-clipboard-check',
    'public': 'fa-user-friends',
    'settings': 'fa-cog'
}
export const getUrl = (path: string) => `${window.location.origin}/${path}`
export const users = {
    [getUrl('data/entity/acr')]: 'acr',
    [getUrl('data/entity/project-owner')]: 'project-owner',
    [getUrl('data/entity/verifier')]: 'verifier',
}


export type ApiResult = {
    success: any,
}

export interface Waitable {
    isWaiting: boolean
}

export function reqButton(waitable: Waitable,
        options: m.RequestOptions<ApiResult> & {url: string},
        label: string,
        then: (result: any) => void): m.Vnode {

    let onclick = async () => {
        waitable.isWaiting = true
        let result = await m.request<ApiResult>(options);
        waitable.isWaiting = false
        if (result.success) {
            then(result.success);
        }
    };

    let button
    if (waitable.isWaiting) {
        button = m('button', {class: 'button is-loading'})
    } else {
        button = m('button', {class: 'button', onclick}, label)
    }

    return button
}

export function signButton(waitable: Waitable, claim: any, who: string, label: string, then: (result: any) => void): m.Vnode {
    return reqButton(waitable, {
        url: getUrl('api/sign'),
        method: 'post',
        data: {
            who,
            what: claim
        }
    }, label, then)
}

export type Project = {
    '@context': string | string[],
    id: string,
    '@type': string | string[],
    claim: any,
    signature?: any[] | any
}

export const docs = [
    "carbon rights",
    "tract location",
    "start date",
    "current ownership",
]
interface Docable {
    checked: boolean[]
}

export const checklist = (state: Docable, prefix: string) => m('div', [
    docs.map((doc, i) => [m('label', {class: 'checkbox'},
        m('input', {type: 'checkbox', value: state.checked[i], onclick: () => state.checked[i] = !state.checked[i]}),
        prefix + doc
    ), m('br')])
])