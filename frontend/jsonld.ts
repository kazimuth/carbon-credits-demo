import _jsonld from 'jsonld/dist/jsonld.min'
import _jsig from 'jsonld-signatures/dist/jsonld-signatures.min'
_jsig.use('jsonld', _jsonld)

export const jsonld = _jsonld;
export const jsig = _jsig;
