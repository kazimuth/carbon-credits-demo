import m from 'mithril'
import { functionComponent } from './util'

import Main from './pages/acr'
import ProjectOwner from './pages/project-owner'
import Verifier from './pages/verifier'
import Public from './pages/public'
import Settings from './pages/settings'
import Broker from './pages/broker'
import Skeleton from './components/skeleton'

m.route(<Element>document.getElementById('root'), '/acr', {
    '/acr': functionComponent(() => m(Skeleton, {page: 'acr'}, m(Main))),
    '/project-owner': functionComponent(() => m(Skeleton, {page: 'project-owner'}, m(ProjectOwner))),
    '/verifier': functionComponent(() => m(Skeleton, {page: 'verifier'}, m(Verifier))),
    '/public': functionComponent(() => m(Skeleton, {page: 'public'}, m(Public))),
    '/settings': functionComponent(() => m(Skeleton, {page: 'settings'}, m(Settings))),
    '/broker': Broker
})