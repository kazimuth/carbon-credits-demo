import m from 'mithril'
import * as LinkedData from '../models/linked-data'
import _ from 'lodash'
import { getUrl, icons, users } from '../util'

type KnownField = {
    icon: string,
    source: string
}

const knownFields: {[field: string]: KnownField} = {
    name: {
        icon: 'fa-user',
        source: 'https://schema.org/name'
    },
    legalName: {
        icon: 'fa-building',
        source: 'https://schema.org/legalName'
    }
}

interface Attrs {
    url?: string,
    data?: any,
    who: string
}

interface State {
    submenu: undefined | 'trust' | 'raw'
}

const SPECIAL_KEYS = ['@id', 'id', '@context', 'proof', 'signature']

const names = {
    [getUrl('data/entity/project-owner')]: 'Project Owner',
    [getUrl('data/entity/verifier')]: 'Verifier',
    [getUrl('data/entity/acr')]: 'ACR',
}

const signedBy = (who: string) => m('p',
    m('i', {class: `fas ${icons[users[who]]} is-${users[who]}`}),
    m.trust('&nbsp;'),
    m('span', names[who])
)

export default {
    oninit: async (v) => {
        if (!v.attrs.data && v.attrs.url) {
            m.redraw()
        }
    },
    view: (v) => {
        let data: any;
        if (v.attrs.url) {
            const outerClaim = LinkedData.claims[v.attrs.url]
            if (outerClaim.loading) {
                return 'gimme a sec'
            }
            data = outerClaim.data
        } else if (v.attrs.data) {
            data = v.attrs.data
        }
        let keys = _.concat(['id'], Object.keys(data.claim).sort()
            .filter(f => SPECIAL_KEYS.indexOf(f) === -1))

        const tags = keys
            .map(k => m('div', {class: 'tag is-black'}, k, m.trust(':&nbsp;'), m('span', {class: 'is-value'}, data.claim[k])))

        let signature;
        if (data.signature) {
            let sigs
            if (_.isArray(data.signature)) {
                sigs = data.signature.map((s: any) => signedBy(s['dc:creator']['id']))
            } else {
                sigs = [signedBy(data.signature['dc:creator']['id'])]
            }
            signature = [m('i', {class: 'fas fa-lock'}), m.trust('&nbsp'), m('span', 'signed by'), sigs]
        } else {
            signature = [m('i', {class: 'fas fa-unlock-alt'}), m.trust('&nbsp'), m('span', 'unsigned claim')]
        }

        var q = '';
        
        return m('div', {class: `notification is-${v.attrs.who}-hl box`}, 
            m('a', {class: 'subsubtitle', href: data.id},
                `Verifiable Claim: `, data['@type'] !== undefined? data['@type'][1] : '',
                ),
            m.trust('&nbsp;'),
            m('a', {class: 'fas fa-download', href: data.id, download: `claim-${data['id'].substr(7)}.json`}),
            m('hr'),
            tags,
            m('hr'),
            signature
        )

    }
} as m.Component<Attrs, State>