import m from 'mithril'
import { hero } from '../util'

export interface Attrs {}
interface State {
}

let verified = false

const data = {
    "@context": "https://schema.org",
    "id": "https://broker.example.org/claims/37",
    "claim": {
        "id": "https://jhgilles.example.org",
        "name": "James Gilles",
        "hasCarbonCredits": [
            {
                "id": "https://acr.example.org/credits/3121938",
                "fraction": 0.00138
            }
        ]
    },
    "https://w3id.org/security#signature": {
        "type": "https://w3id.org/security#LinkedDataSignature2015",
        "dc:created": {
            "type": "xsd:dateTime",
            "@value": "2018-08-20T03:21:48Z"
        },
        "dc:creator": {
            "id": "https://acr.example.org"
        },
        "https://w3id.org/security#domain": "https://acr.example.org",
        "https://w3id.org/security#signatureValue": "gCfTZ8knWkXC+JeOgH5gXzekTi2l0PNh+I3qg9lcSGvIgrh7tcUeeVA8rCe3olOHGDgVCa+beJ3kLB/p5XijIOJUvhNOzFXJ0VSUIp1U0bye3D0E0iHfYquAY1Gk4JMEMbSF/ZNzQYCA7LsO4bm5kqC6gIam2WIxllSROrRKsG9x6MPAei+2pwltZwfQ7WGOVIloj2Hs76JWZr4ox3vPVJDKqrFSrt2rCx9tCIDD9cvOiHaj6NC1zJIHuTFLL+JYieJwuVbyvZzWnmZuCvxmnLU32r6Ila1chuNiKZpl6HGmmtd6QV6Blr3yOYwLcW90fiEd2nJsPLSovAcmVWxgDw=="
    }
};

export default {
    view(vnode) {
        return m('div', [
            hero('MEMBER OF THE PUBLIC', `You're a member of the public`, 'public'),
            m('div', {class: 'columns'}, [
                m('div', {class: 'column'}, [
                    m('button', {class: 'button is-alert', onclick: () => verified = true}, 'verify'),
                    verified? m('p', 'Verified!') : m('')
                ]),
                m('div', {class: 'column'}, m('pre', JSON.stringify(data, undefined, '    '))),
//m('code', JSON.stringify(data, undefined, '    ')))),
            ])
        ])
    }
} as m.Component<Attrs, State>