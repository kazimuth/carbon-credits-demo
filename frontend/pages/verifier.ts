import m from 'mithril'
import { hero, getUrl, Project, signButton, reqButton, docs, checklist } from '../util'
import LdCard from '../components/ld-card'
import { jsig } from '../jsonld'
import _ from 'lodash'

export interface Attrs {}
interface State {
    isWaiting: boolean,
    project?: Project,
    verification?: Project,
    finished: boolean,
    checked: boolean[]
}

const verification = {
    '@context': [
        jsig.SECURITY_CONTEXT_URL,
        "https://schema.org/"
    ],
    id: getUrl('data/projects/1/claims/verification'),
    '@type': ['VerifiableClaim', 'Carbon Verification'],
    claim: {
        id: getUrl('data/projects/1'),
        verifiedBy: getUrl('data/entity/verifier'),
        verifies: getUrl('data/projects/1/claims/initial')
    }
}

export default {
    oninit(vnode) {
        vnode.state.checked = docs.map(_.constant(false))
        vnode.state.isWaiting = true
        m.request<any>({
            url: getUrl('data/projects/1/claims/initial'),
            method: 'get'
        }).then(result => {
            console.log('initial')
            vnode.state.project = result
            m.request<any>({
                url: getUrl('data/projects/1/claims/verification'),
                method: 'get'
            }).then(result => {
                console.log('verfication fnd')
                for (var i = 0; i < vnode.state.checked.length; i++) {
                    vnode.state.checked[i] = true
                }
                vnode.state.verification = result
                vnode.state.isWaiting = false
                vnode.state.finished = true
                m.redraw()
            }).catch(err => {
                vnode.state.isWaiting = false
                m.redraw()
            });

        }).catch(err => {
            console.log('no claim yet')
            vnode.state.isWaiting = false
            m.redraw()
        })
    },
    view(vnode) {
        // projects to verify
        let sign
        if (!vnode.state.verification && _.every(vnode.state.checked)) {
            vnode.state.verification = verification
        }
        const p = vnode.state.verification
        const storeResult = (result: any) => vnode.state.verification = result
        if (vnode.state.finished) {
            sign = m('p', 'Done with project!')
        } else if (p) {
            if (p.signature) {
                if (p.signature.length && p.signature.length === 2) {
                    sign = reqButton(vnode.state, {
                        url: getUrl('api/store'),
                        method: 'post',
                        data: {
                            what: p
                        }
                    }, 'Send Back to Project Owner', () => {
                        vnode.state.finished = true
                    })
                } else {
                    sign = signButton(vnode.state, p, getUrl('data/entity/acr'), 'Submit to ACR for Validation', storeResult)
                }
            } else {
                sign = signButton(vnode.state, p, getUrl('data/entity/verifier'), 'Sign Claim', storeResult)
            }
        }

        return m('div', [
            hero('VERIFIER', `You're an accredited carbon verifier`, 'verifier'),
            m('div', {class: 'columns'}, [
                m('div', {class: 'column'}, vnode.state.project?
                    m(LdCard, {data: vnode.state.project, who: 'project-owner'}) :
                    m('p', 'Waiting for a project to verify...')),
                m('div', {class: 'column'}, vnode.state.project?
                    m('div', {class: 'section'},
                        m('p', 'Please validate the following documents:'),
                        m('br'),
                        checklist(vnode.state, 'Validate '),
                    )
                    :
                    m('div')),
                m('div', {class: 'column'},
                    vnode.state.verification? m(LdCard, {data: vnode.state.verification, who: 'verifier'}) : '',
                    sign
                ),
            ])
        ])
    }
} as m.Component<Attrs, State>