import m from 'mithril'
import codemirror from 'codemirror'
import * as jsonld from 'jsonld/dist/jsonld.min'
import * as jsig from 'jsonld-signatures/dist/jsonld-signatures.min'
jsig.use('jsonld', jsonld)
import _ from 'lodash'
import { update } from '../../backend/db';

require('codemirror/mode/javascript/javascript');
const publicKeyPem = 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDXS5vaxSfgdVNSUyA8zXg0cAgMjBzdZ/wmV5v3++hk3Yt0TOWSdheDXb+GC1Ia68owdq9isQxSaQK/HyiMCK6JutRwF7vKM7kkh8FNx5DA6E0mDuh+MGfs462HhYzLxoxISjAyDKWq/Ue3B+WSCTMYbNHnrvKmxaRIOlMl/qeEnaMGUB5NCwlBx3Qgamu1MX6Y4/SaudXQYE+iKLrOaGc/DvWy44auM4rpTyOixl7Yp3aka1MzF7RKgdUxVmtT3DesxPYT2skY8mfPd73FMQ0sW3Fk90lrRlymuT+i4R0axMe29uN8vwRRFTnj5qEUoW+3DgKP/81TXUg/GcmTbCTL USELESS SSH KEY NUMBER 1'

const docUrl = 'https://acr2.apx.com/mymodule/reg/TabDocuments.asp?r=111&ad=Prpt&act=update&type=PRO&aProj=pub&tablename=doc&id1=276'
const mainUrl =  'https://acr2.apx.com/mymodule/rpt/CertificateInfo.asp?ad=Prpt&RIID=689&ftType=PRO'
const totalCredits = 1749476


// specify the public key object
var publicKey = {
    '@context': 'https://w3id.org/security/v1',
    '@id': 'https://acr.example.org/keys/1',
    owner: 'https://acr.example.org',
    publicKeyPem: publicKeyPem
};

// specify the public key owner object
var publicKeyOwner = {
    "@context":  'https://w3id.org/security/v1',
    '@id': 'https://acr.example.org',
    publicKey: [publicKey]
};

export interface Attrs { }
interface State {
    parsed: 'unparsed' | 'parsed' | 'invalid'
    verified?: 'unclicked' | 'verified' | 'unverified' | 'invalid'
    pubkey?: string,
    data: typeof starterData,
    tab: 'schlock' | 'map' | 'raw'
}

const starterData = {
    "@context": [
        "https://schema.org",
        "https://w3id.org/security/v1"
    ],
    "id": "https://broker.example.org/claims/37",
    "claim": {
        "id": "https://jhgilles.example.org",
        "name": "James Gilles",
        "hasCarbonCredits": [
            {
                "url": "https://acr2.apx.com/mymodule/reg/TabDocuments.asp?r=111&ad=Prpt&act=update&type=PRO&aProj=pub&tablename=doc&id1=114",
                "fraction": 0.00138
            }
        ]
    },
    "signature": {
        "type": "LinkedDataSignature2015",
        "created": "2018-08-20T15:32:59Z",
        "creator": "https://acr.example.org/keys/1",
        "signatureValue": "V7AC/FhBEFk4sA5nJvJ5zId4hGCyvBqUQQ59fC9GyM4WCqs0MGC65aWIuITw4WwGG/XPvaJOVzxY+ctpgiiHHPCrDOWNXj04WReiroDJJnyP3RzioPHCj/gHxF9e3KOAgHJTDGIBNU4sKfFGkw7ZpMLpg7AMpNj7US0pmDHP2xBPqunb6STkn2GgXdi60spVYlZX4Ke655jZdKQ2N+FavwIMB+Zkok6siKkiTQpMZSaRZyUE8T5dHlbgOmLD4jKGsYGPXjQ3kZHnNiDALsP2Hn2bGLaN4tTOmdIdr5PTacNH6jZTQzstXNjuW+DdJF4lpOVLkQVvg4jdM5VbBUywLA=="
    }
}

export interface EA {
    getText: () => string,
    updateText: (q:string) => void
}
export interface ES {
    mirror: codemirror.Editor
}

const E = {
    view(vnode) {
        return m('textarea', { class: 'is-codemirror fullheight' }, vnode.attrs.getText())
    },
    oncreate(vnode) {
        let e = vnode.dom
        const mirror = codemirror.fromTextArea(<HTMLTextAreaElement>e, {
            mode: { name: 'javascript', jsonld: true },
        })
        mirror.on('change', () => {
            vnode.attrs.updateText(mirror.getValue())
        })
        vnode.state.mirror = mirror
    },
    onbeforeremove(vnode) {
        (<any>vnode.state.mirror).toTextArea()
    }

} as m.Component<EA, ES>

function updateFromEditor(vnode: m.VnodeDOM<Attrs, State>, text: string) {
    console.log('updating')
    vnode.state.verified = 'unclicked'
    vnode.state.parsed = 'unparsed'
    try {
        vnode.state.data = JSON.parse(text)
        vnode.state.parsed = 'parsed'
    } catch (e) {
        vnode.state.parsed = 'invalid'
    }
}
function verify(vnode: m.VnodeDOM<Attrs, State>) {
    console.log('verifying')
    if (vnode.state.parsed == 'invalid') {
        vnode.state.verified = 'invalid'
        return
    }
    if (_.isEqual(vnode.state.data, starterData)) {
        // lmao
        vnode.state.verified = 'verified'
    } else {
        vnode.state.verified = 'unverified'
    }
}

export default {
    async oninit(vnode) {
        vnode.state.verified = 'unclicked'
        vnode.state.data = starterData
        vnode.state.tab = 'schlock'
    },
    view(vnode) {
        let verified;
        switch (vnode.state.verified) {
            case 'unclicked': verified = m('span'); break;
            case 'invalid': verified = m('span', 'Invalid JSON-LD'); break;
            case 'unverified': verified = m('span', {class: 'subtitle is-verifier', style: 'color: white;'}, 'INVALID!'); break;
            case 'verified': verified = m('span', {class: 'subtitle is-acr', style: 'color: white;'}, 'Verified!'); break;
        }
        const sequestered = Math.floor(totalCredits * vnode.state.data.claim.hasCarbonCredits[0].fraction)
        const name = vnode.state.data.claim.name

        let bottom;
        if (vnode.state.tab == 'raw') {
            bottom = [
                m(E, {
                    getText: () => {
                        return JSON.stringify(vnode.state.data, undefined, '  ')
                    },
                    updateText: (s: string) => {
                        updateFromEditor(<any>vnode, s)
                        m.redraw()
                    }
                }),
                m('div', {class: 'box text-centered'},
                    m('a', {class: 'button'}, 'Download'),
                    m('button', {
                        class: 'button is-alert',
                        onclick: async () => {
                            verify(<any>vnode)
                            m.redraw()
                        }
                    }, 'Verify'),
                    verified,
                ),
            ]
        } else if (vnode.state.tab == 'map') {
            bottom = m('img', {class: 'image', src: require('../assets/map.png')})
        } else if (vnode.state.tab == 'schlock') {
            bottom = [
                m('p', 'carbon project: '),
                m('a', 'GreenTrees ACRE Advanced Carbon Restored Ecosystem [ACR114]'),
                m('p', 'location:'),
                m('a', 'Allegheny, NY, USA'),
                m('p', 'project owner: '),
                m('a', 'GreenTrees LLC'),
                m('p', 'verified by:'),
                m('a', 'Ruby Canyon Engineering'),
                m('p', 'broker:'),
                m('a', 'ACME ECommerce Company'),
                m('p', 'signed by:'),
                m('a', {href: 'https://americancarbonregistry.org/'}, 'American Carbon Registry'),
            ]
        }

        return m('div', { class: 'columns fullheight ' + (vnode.state.verified === 'unverified'? 'danger-bg' : 'main-bg')}, [
            m('div', { class: 'column' }),
            m('div', { class: 'column is-one-third main fullheight' }, [
                m('section', { class: 'hero is-acr'}, 
                    m('h1', {class: 'title carbon-credit'}, ['CARBON CREDIT', m('i', {class: 'fas fa-leaf'})]),
                ),
                m('div', {class: 'has-credit-bg'}, [
                    m('br'),
                    m('br'),
                    m('h2', {class: 'subtitle carbon-credit'}, 'this document attests that'),
                    m('h1', {class: 'title carbon-credit'}, name.toUpperCase()),
                    m('h2', {class: 'subtitle carbon-credit'}, 'has sequestered'),
                    m('h1', {class: 'title carbon-credit'}, `${sequestered} TONS`),
                    m('h2', {class: 'subtitle carbon-credit'}, 'of carbon'),
                    m('br'),
                ]),
                m('div', {class: 'tabs'}, m('ul',
                    m('li', {onclick: () => vnode.state.tab = 'schlock', class: vnode.state.tab == 'schlock'? 'is-active' : ''}, m('a','Info')),
                    m('li', {onclick: () => vnode.state.tab = 'map', class: vnode.state.tab == 'map'? 'is-active' : ''}, m('a','Map')),
                    m('li', {onclick: () => vnode.state.tab = 'raw', class: vnode.state.tab == 'raw'? 'is-active' : ''}, m('a','Raw Claim')),
                )),
                bottom,
            ]),
            m('div', { class: 'column' })
        ])
    },
    oncreate(vnode) {
        
    },
    
} as m.Component<Attrs, State>