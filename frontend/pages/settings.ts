import m from 'mithril'
import { hero, reqButton, getUrl } from '../util'

export interface Attrs {}
interface State {
    isWaiting: boolean
}

export default {
    view(vnode) {
        return m('div', [
            hero('SETTINGS', `Demo Settings`, 'settings'),
            reqButton(vnode.state, {url: getUrl('api/reset'), method: 'post'}, 'Reset Demo (takes ~10 seconds)', () => 0)
        ])
    }
} as m.Component<Attrs, State>