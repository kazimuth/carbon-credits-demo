import m from 'mithril'
import { hero, getUrl } from '../util'
import LdCard from '../components/ld-card'

export interface Attrs {}
interface State {}

export default {
    view(vnode) {
        return m('div', [
            hero('CARBON CREDITS DEMO', 'with Verifiable Claims', 'acr'),
            m('p', {class: 'has-padding'}, 'I am a...'),
            m('section', m('div', {class: 'columns'}, [
                m('div', {class: 'column is-3'}, m('div', {class: 'section'}, m('a', {href: '#!/project-owner'}, 'Project owner'))),
                m('div', {class: 'column is-3'}, m('div', {class: 'section'}, m('a', {href: '#!/verifier'}, 'Verifier'))),
                m('div', {class: 'column is-3'}, m('div', {class: 'section'}, m('a', {href: '#!/public'}, 'Member of the public'))),
            ])),
        ])
    }
} as m.Component<Attrs, State>