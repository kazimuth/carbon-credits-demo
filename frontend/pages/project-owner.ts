import m from 'mithril'
import { Project, reqButton, signButton, hero, getUrl, docs, checklist } from '../util'
import LdCard from '../components/ld-card'
import _ from 'lodash'

/// Submit-to-acr step is called "validating"
/// Subtitle: Proof of Title to Offsets

/// project proponent shall provide docs / assets [to verifier] of undisputed titles to all offsets prior to verification
/// transferred offset titles if not owned

/// once verifier verifies, ...

/// add checklist of documents for both

/// The documentation includes
/// Deeds demonstrating land ownership,
/// Contracts between the landowner and environmental project owner providing title to carbon
/// offsets to the environmental project owner
/// Proof of paid up current year taxes for the land
/// Other documentation such as easements for the environmental project may be included

import { jsig } from '../jsonld'

const project = {
    '@context': [
        jsig.SECURITY_CONTEXT_URL,
        "https://schema.org/"
    ],
    id: getUrl('data/projects/1/claims/initial'),
    '@type': ['VerifiableClaim', 'Carbon Project'],
    claim: {
        name: 'Project 1',
        id: getUrl('data/projects/1'),
        location: 'Schenechtedy, NY',
        offsetOwner: 'GreenTrees',
        carbonRightsDocs: 'https://example.org/grt178/GRT178_Willow_Break_Contract.pdf',
        tractLocationDocs: 'https://example.org/grt178/WRP_Compliance_Cert_GRT_178.pdf',
        startDateDocs: 'https://example.org/grt178/0178_ForestryAssessment_plantingDate.pdf',
        currentOwnershipDocs: 'https://example.org/grt178/GRT178_Current_Ownership.pdf'
    }
}

const isSigned = (claim: any) => claim.signature !== undefined

export interface Attrs {}
interface State {
    projects: Project[],
    selected?: number,
    isWaiting: boolean,
    status: 'start' | 'awaitverify' | 'done',
    checked: boolean[]
}

const renderProject = (s: State, p: Project, i: number) => {
    return [m('button', {class: 'button', onclick: () => selectProject(s, i)}, p.claim.name), m('br')]
}

const addProject = (s: State) => {
    s.projects.push(project)
    selectProject(s, s.projects.length - 1)
}
const selectProject = (s: State, n: number) => {
    s.selected = n
}

export default {
    oninit(vnode) {
        vnode.state.checked = docs.map(() => false);
        vnode.state.projects = []
        vnode.state.isWaiting = true
        vnode.state.status = 'start'
        m.request<any>({
            url: getUrl('data/projects/1/claims/initial'),
            method: 'get'
        }).then(result => {
            vnode.state.projects.push(result)
            vnode.state.selected = 0
            vnode.state.isWaiting = false
            vnode.state.status = 'awaitverify'
            for (var i = 0; i < vnode.state.checked.length; i++) {
                vnode.state.checked[i] = true
            }
            m.redraw()
        }).catch(err => {
            console.log('no claim yet')
            vnode.state.isWaiting = false
            m.redraw()
        })
    },
    view(vnode) {
        // PROJECTS // DATA
        // [new]      [junk] [self-sign] \/
        //                   [submit] ... [acr-signed]
        
        let selectedViz;
        if (vnode.state.selected !== undefined) {
            const p = vnode.state.projects[vnode.state.selected]

            const storeResult = (r: any) => vnode.state.projects[<any>vnode.state.selected] = r

            let sign
            if (vnode.state.status === 'start') {
                if (p.signature) {
                    if (p.signature.length && p.signature.length === 2) {
                        sign = reqButton(vnode.state, {
                            url: getUrl('api/store'),
                            method: 'post',
                            data: {
                                what: p
                            }
                        }, 'Send to Verifier', () => {
                            vnode.state.status = 'awaitverify'
                        })
                    } else {
                        sign = signButton(vnode.state, p, getUrl('data/entity/acr'), 'Submit to ACR for Validation', storeResult)
                    }
                } else {
                    sign = signButton(vnode.state, p, getUrl('data/entity/project-owner'), 'Sign Claim', storeResult)
                }
            } else if (vnode.state.status === 'awaitverify') {
                sign = m('p', 'Waiting for project verification...')
            }

            selectedViz = [
                m('div', {class: 'column'}, 'Add land ownership documentation here:', checklist(vnode.state, 'Add ')),
                m('div', {class: 'column'}, _.every(vnode.state.checked) ? [
                    m(LdCard, { data: p, who: 'project-owner' }),
                    sign
                ] : '')
            ]
        } else {
            selectedViz = [
                m('div', {class: 'column'}),
                m('div', {class: 'column'})
            ]
        }

        return m('div', [
            hero('PROJECT OWNER', `You're the owner of the project, and you want credit for planting trees`, 'project-owner'),
            m('div', {class: 'columns'}, [
                m('div', {class: 'column'},
                    vnode.state.projects.map((p, i) => renderProject(vnode.state, p, i)),
                    vnode.state.projects.length == 0 ? m('button', {class: 'button', onclick: () => addProject(vnode.state) }, 'Create Project') : ''
                ),
                selectedViz
            ])
        ])
    }
} as m.Component<Attrs, State>