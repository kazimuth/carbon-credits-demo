FROM mhart/alpine-node

COPY package.json package-lock.json /app/
RUN cd /app &&\
    apk add --no-cache git python make g++ build-base &&\
    npm install --production --no-optional &&\
    apk del git python make g++ build-base 

COPY useless-keys /app/useless-keys
COPY dist /app/dist
COPY backend-dist /app/backend-dist

WORKDIR /app
ENV VC_DEMO_DBFILE /data/vc-demo.nedb
CMD ["node", "backend-dist/index.js"]
