/// <reference path="./shared/jsonld.d.ts"/>

import * as jsonld from 'jsonld'
import { json } from './node_modules/@types/body-parser'
import * as jsig from 'jsonld-signatures'
jsig.use('jsonld', jsonld)
import { promisify } from 'util'
import * as fs from 'fs'
const readFile = promisify(fs.readFile)


let q = {
    '@context': ['https://schema.org', 'https://w3id.org/security/v1'],
    'id': 'https://broker.example.org/claims/37',
    'claim': {
        'id': 'https://jhgilles.example.org',
        'name': 'James Gilles',
        'hasCarbonCredits': [
            {'url': 'https://acr2.apx.com/mymodule/rpt/CertificateInfo.asp?ad=Prpt&RIID=689&ftType=PRO', 'fraction': 0.00138}
        ]
    }
};

(async () => {
    const signed = await jsig.sign(<jsonld.JsonLd>(<any>q),
    {
        algorithm: 'LinkedDataSignature2015',
        privateKeyPem: await readFile('useless-keys/key1', 'utf8'),
        //domain: 'https://acr.example.org',
        creator: 'https://acr.example.org/keys/1'
    });
    console.log(JSON.stringify(signed));
})()