
declare module 'jsonld' {
    interface JsonLd {
        [key: string]: string | JsonLd
    }
    interface JsonLdContext {
        [key: string]: string | JsonLdContext
    }
    interface JsonLdFrame {
        [key: string]: string | JsonLdFrame
    }
    interface Cb {
        (err: Error | undefined, result: JsonLd | undefined): void
    }
    interface StringCb {
        (err: Error | undefined, result: string | undefined): void
    }
    type Url = string
    interface Opts {
        documentLoader?: (url: Url, callback: any) => Promise<JsonLd>
    }

    function compact(
        doc: JsonLd | Url,
        context: JsonLdContext | Url,
        cb?: Cb,
        opts?: Opts
    ): Promise<JsonLd>
    function expand(
        doc: JsonLd | string,
        cb?: Cb,
        opts?: Opts,
    ): Promise<JsonLd>
    function flatten(
        doc: JsonLd,
        cb?: Cb,
        opts?: Opts,
    ): Promise<JsonLd>
    function frame(
        doc: JsonLd,
        frame: JsonLdFrame | Url,
        cb?: Cb,
        opts?: Opts,
    ): Promise<JsonLd>
    function canonize(
        doc: JsonLd,
        opts?: Opts & {algorithm?: 'URDNA2015' | 'URGNA2012', format: 'application/n-quads'},
        cb?: StringCb,
    ): Promise<string>
    function normalize(
        doc: JsonLd,
        opts?: Opts & {algorithm?: 'URDNA2015' | 'URGNA2012', format: 'application/n-quads'},
        cb?: StringCb,
    ): Promise<string>
    function toRDF(
        doc: JsonLd,
        opts: Opts & {algorithm?: 'URDNA2015' | 'URGNA2012', format: 'application/n-quads'},
        cb?: StringCb,
    ): Promise<string>
    function fromRDF(
        doc: JsonLd,
        spec: {algorithm: 'URDNA2015', format: 'application/n-quads'},
        cb?: Cb,
        opts?: Opts,
    ): Promise<JsonLd>
    var documentLoaders: {
        node: () => (url: String, callback: any) => void
        xhr: (opts: {usePromise: false}) => (url: String, callback: any) => Promise<JsonLd> | undefined
    }
    var documentLoader: (url: String, callback: any) => Promise<JsonLd> | undefined
}

declare module 'jsonld/dist/jsonld.min' {
    import * as jsonld from 'jsonld'
    type JsonLd = jsonld.JsonLd
    type Url = jsonld.Url
    type JsonLdContext = jsonld.JsonLdContext
    type JsonLdFrame = jsonld.JsonLdFrame
    type Cb = jsonld.Cb
    type StringCb = jsonld.StringCb
    type Opts = jsonld.Opts
    const compact: typeof jsonld.compact
    const expand: typeof jsonld.expand
    const flatten: typeof jsonld.flatten
    const frame: typeof jsonld.frame
    const canonize: typeof jsonld.canonize
    const normalize: typeof jsonld.normalize
    const toRDF: typeof jsonld.toRDF
    const fromRDF: typeof jsonld.fromRDF
    const documentLoaders: typeof jsonld.documentLoaders
    const documentLoader: typeof jsonld.documentLoader
}


declare module 'jsonld-signatures' {
    import { JsonLd, Url, Opts } from 'jsonld'

    type SecurityContextUrl = Url
    const SECURITY_CONTEXT_URL: SecurityContextUrl

    function use(_: 'jsonld', jsonld: any): void

    function sign(
        input: JsonLd,
        opts: Opts & {
            algorithm: "EcdsaKoblitzSignature2016" |
                "Ed25519Signature2018" |
                "GraphSignature2012" |
                "RsaSignature2018" | 
                "LinkedDataSignature2015",
            privateKeyWif?: string,
            publicKeyWif?: string,
            privateKeyBase58?: string,
            publicKeyBase58?: string,
            privateKeyPem?: string,
            publicKeyPem?: string,
            creator?: Url,
            date?: Date,
            domain?: Url,
            nonce?: String,
            proof?: JsonLd,
        }
    ): Promise<JsonLd>

    function verify(
        input: JsonLd,
        opts: Opts & {
            publicKey?: JsonLd | ((keyId: Url, options: any) => Promise<JsonLd>),
            publicKeyOwner?: JsonLd | ((keyId: Url, options: any) => Promise<JsonLd>),
            checkNonce?: (nonce: string | null, options: any) => Promise<boolean>,
            checkDomain?: (domain: Url | null, options: any) => Promise<boolean>,
            checkKey?: (key: string, options: any) => Promise<boolean>,
            checkKeyOwner?: (owner: string, key: string | null, options: any) => Promise<boolean>,
            checkTimestamp?: boolean,
            // in seconds
            maxTimestampDelta?: number,
            id?: string
        }
    ): Promise<boolean>
}

declare module 'jsonld-signatures/dist/jsonld-signatures.min' {
    import * as jsig from 'jsonld-signatures'
    const use: typeof jsig.use
    const sign: typeof jsig.sign
    const verify: typeof jsig.verify
    const SECURITY_CONTEXT_URL: typeof jsig.SECURITY_CONTEXT_URL
}