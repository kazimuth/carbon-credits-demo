import * as jsig from 'jsonld-signatures'
import * as fs from 'fs'
import * as path from 'path'
import { promisify } from 'util'

import * as db from './db'
import * as _ from 'lodash'
import log from './log'
import { sign } from './security'

/// routines to add initial data to the database

const readFile_ = promisify(fs.readFile)

const read: {[file: string]: string} = {}
const readFile = async (path: string, fmt: string) => {
    if (read[path] !== undefined) {
        return read[path]
    } else {
        read[path] = await readFile_(path, fmt)
        return read[path]
    }
}

const ACR = db.getDataUrl('data/entity/acr');
const insertEntity = async (shortId: string, extras: {[k: string]: any}, keyNumber: number, bootstrap?: boolean) => {
    const id = db.getDataUrl(`data/entity/${shortId}`)
    log.info(`Adding entity ${id} with key # ${keyNumber} and extra attributes ${JSON.stringify(extras)}`)
    const entity = _.assign({
        '@context': [
            jsig.SECURITY_CONTEXT_URL,
            'https://schema.org/'
        ],
        'id': id,
        'publicKey': [db.getDataUrl(`data/entity/${shortId}/keys/1`)]
    })
    const key = {
        '@context': [
            jsig.SECURITY_CONTEXT_URL,
            'https://schema.org/'
        ],
        'id': db.getDataUrl(`data/entity/${shortId}/keys/1`),
        'owner': id,
        'publicKeyPem': await readFile(path.join(path.dirname(__dirname), 'useless-keys', `key${keyNumber}.pub`), 'utf8')
    }
    // no `@id` field, so this can't be externally queried
    // should use an actual keystore if this is ever deployed somehow
    const privKey = {
        'privateKeyOwner': id,
        'privateKey': await readFile(path.join(path.dirname(__dirname), 'useless-keys', `key${keyNumber}`), 'utf8')
    }
    if (bootstrap) {
        // sign our own entity if we're the ACR
        await db.insert(await sign(ACR, entity, privKey.privateKey))
        await db.insert(await sign(ACR, key, privKey.privateKey))
    } else {
        // have the ACR signify this is a valid account, as well as our public key
        await db.insert(await sign(ACR, entity))
        await db.insert(await sign(ACR, key))
    }
    // no need to sign private keys
    await db.insert(privKey)
}
export const insertInitialEntities = async () => {
    if (!await db.findOne({'id': db.getDataUrl('data/entity/acr')})) {
        log.info(`Populating initial entities in the database`)
        // insert the ACR first, so that we can sign other things with its keys
        await insertEntity('acr', {legalName: 'American Carbon Registry'}, 1, true) // self-sign this entity

        await Promise.all([
            insertEntity('project-owner', {name: 'Project Owner Gina'}, 2),
            insertEntity('verifier', {name: 'Joe Verifier'}, 3),
            insertEntity('public', {name: 'Ell Publique'}, 4)
        ])
    }
}