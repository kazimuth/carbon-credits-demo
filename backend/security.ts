/// <reference path='../shared/jsonld.d.ts'/>

import * as db from './db'
import * as assert from 'assert'
import * as jsig from 'jsonld-signatures'
import log from './log'

export const sign = async (who: string, what: any, key?: string) => {
    if (!who) {
        log.error(`??? ${who} ${what} ${key}`)
    }

    if (!key) {
        const keyDoc = await db.findOne({ privateKeyOwner: who })
        assert(keyDoc, `no key found for ${who}`)
        key = keyDoc.privateKey
    }

    log.info(`sign ${what.id} by ${who}`)

    return await jsig.sign(what, {
        algorithm: 'LinkedDataSignature2015',
        privateKeyPem: key,
        domain: 'json-ld.org',
        creator: who
    })
}
