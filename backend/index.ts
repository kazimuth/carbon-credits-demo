import * as express from 'express'
import { RequestHandler } from 'express'
import * as jsonld from 'jsonld'
import * as jsig from 'jsonld-signatures'
import { URL } from 'url'
import * as path from 'path'
import * as crypto from 'crypto'
import * as _ from 'lodash'
import * as assert from 'assert'

import log from './log'
import argv from './argv'
import * as db from './db'
import { sign } from './security'
import { insertInitialEntities } from './populate'

log.info('startup')

log.info('load jsig')
jsig.use('jsonld', jsonld)

log.info('inserting test data')

log.info('cfg router')
const app = express()


// assets
const dist = path.join(path.dirname(__dirname), 'dist')

// index
app.get('/', (req, res) => {
    res.sendFile(path.join(dist, 'index.html'))
})
app.use('/static', express.static(dist))

app.use(express.json())

// catch errors in our async handlers
const asyncHandler: (q: RequestHandler) => RequestHandler = fn => (req, res, next) => {
    Promise.resolve(fn(req, res, next))
      .catch(next)
}

// json-ld
app.route('/data*').get(asyncHandler(async (req, res) => {
    const id = db.getDataUrl(req.path)
    const data = await db.findOne({ 'id': id }, {})
    res.type('application/json')
    if (data) {
        log.info(`get data: ${req.path}: found`)
        delete data._id
        res.send(JSON.stringify(data, undefined, ' '))
    } else {
        log.warn(`get data: ${req.path}: NOT found`)
        res.status(404)
        res.send({error: `no such document: ${req.path}`})
    }
}))


// usage: POST /api/store { "what": verifiable-claim }
app.route('/api/store').post(asyncHandler(async (req, res) => {
    assert(req.body)
    assert.strictEqual(typeof req.body.what, 'object')
    log.info(`storing ${req.body.what.id}`)

    await db.update({id: req.body.what.id}, req.body.what, {upsert: true})

    res.send({
        success: req.body
    })
}));

// usage: POST /api/sign { "who": entity-url, "what": verifiable-claim }
app.route('/api/sign').post(asyncHandler(async (req, res) => {
    assert(req.body)
    assert.strictEqual(typeof req.body.who, 'string')

    res.send({
        success: await sign(req.body.who, req.body.what)
    })
}));

// usage: POST /api/reset { } clear and re-populate database
app.route('/api/reset').post(asyncHandler(async (req, res) => {
    log.info('clearing')
    await db.clear()
    log.info('repopulating')
    await insertInitialEntities()
    log.info('done')

    res.send({
        success: true
    })
}));

(async () => {
    await insertInitialEntities()
    log.info(`listening forever on ${argv.port}...`);
    app.listen(argv.port)
})()

